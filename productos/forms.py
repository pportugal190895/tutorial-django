from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from productos.models import Producto, Categoria

CATEGORIAS = (
    ('dulces', 'dulces'),
    ('ropa', 'ropa'),
)

class ProductoCreateForm(forms.Form):
    nombre = forms.CharField(max_length=200)
    precio = forms.FloatField()
    stock = forms.IntegerField()
    categoria = forms.ChoiceField(choices=CATEGORIAS)

    def is_valid(self):
        categoria = Categoria.objects.filter(nombre__exact=self.data.get('categoria')).first()
        try:

            producto = Producto.objects.create(
                nombre=self.data.get('nombre'),
                precio=self.data.get('precio'),
                stock=self.data.get('stock'),
                categoria=categoria
            )
            producto.save()
            return True
        except:
            return ValidationError('Invalid value')


class ProductCreateModelForm(ModelForm):
    class Meta:
        model = Producto
        fields = ['nombre', 'precio']
