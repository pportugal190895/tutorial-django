from django.urls import path
from productos.views import ListaProductosListView, SumaDeStocksView, BuscarProducto, CrearProducto

urlpatterns = [
    path('lista/productos/', ListaProductosListView.as_view(), name="listarProductos"),
    path('suma/stocks/', SumaDeStocksView.as_view(), name='sumaStocks'),
    path('busqueda/productos/<slug:busqueda>/', BuscarProducto.as_view(), name='busqueda'),
    path('busqueda/productos/', BuscarProducto.as_view(), name='busqueda'),
    path('crear-producto/', CrearProducto.as_view(), name='crear-producto'),
]