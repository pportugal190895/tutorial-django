from django.forms import ModelForm
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView, ListView, FormView
from productos.forms import ProductoCreateForm, ProductCreateModelForm
from productos.models import Producto, Categoria


class HomeView(TemplateView):
    template_name = "home.html"


class ListaProductosListView(ListView):
    model = Producto
    template_name = 'lista-productos.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        contexto = super(ListaProductosListView, self).get_context_data(object_list=None, **kwargs)
        productos = Producto.objects.all()
        categorias = Categoria.objects.all()
        contexto['productos'] = productos
        contexto['categorias'] = categorias
        return contexto

class SumaDeStocksView(View):

    def get(self, request):
        productos = Producto.objects.all()
        suma = 0
        for producto in productos:
            suma += producto.stock
        context = {'suma': suma}
        return render(request, 'suma-stocks.html', context=context)

class BuscarProducto(View):

    def get(self, request, busqueda=None, *args, **kwargs):
        if not busqueda:
            querySearch = Producto.objects.all()
            print("no hay busqueda")
        else:
            print(busqueda)
            querySearch = Producto.objects.filter(nombre__contains=busqueda)

        contexto = {'querySearch': querySearch}
        return render(request, 'buqueda.html', context=contexto)

    def post(self, request):
        entrada_busqueda = request.POST.get('query')
        kwargs = {'busqueda': entrada_busqueda}
        return HttpResponseRedirect(reverse(
            'busqueda', kwargs=kwargs))


class CrearProducto(FormView):
    form_class = ProductCreateModelForm
    template_name = 'crear-producto.html'

    def form_valid(self, form):
        return HttpResponseRedirect(reverse('home'))